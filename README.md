CF-MYSQL Deployment Template
======================================

This repository acts as an upstream repository of YAML templates for use
 deploying one or more CF-MYSQL, via the [Gensis][1] utility.



Creating a new CF-MYSQL Deployment
======================================

To create a new [Genesis][1]-based deployment of CF-MYSQL, run

    genesis new deployment --template https://bitbucket.org/canopy-system-user/cf-mysql-deployment.git

This will create a new repo called `cf-mysql-deployments` for you, and
pull in the `https://bitbucket.org/canopy-system-user/cf-mysql-deployment.git` repo as the
`upstream` remote, copying the contents of `global/*` into the new
`cf-mysql-deployments` repository.

This allows you to easily diverge from the upstream templates to suit your
environment, yet still be able to pull in changes from upstream down
the road.


BOSH-LITE Sites
======================================

The `bosh-lite` template will set you up with a structure suitable
for deploying cf-mysql on a bosh-lite.

    genesis new site --template bosh-lite <name>


Amazon EC2 (AWS) Sites
======================================

The `aws` template will set you up with a structure suitable for
deploying BOSH Lites to Amazon Web Service's EC2/VPC
infrastructure.

    genesis new site --template aws <name>


Notes
======================================

For more information, check out the [Genesis][1] repo, or `genesis help`.
You can download the Genesis program from [Github][1]


[1]: https://github.com/starkandwayne/genesis
